# Compasso UOL API Test

##### API desenvolvida como teste para vaga de desenvolvedor

## Aplicação

* SpringBoot + H2
* Maven
* Open API

## Build

###### Para build via comando *mvn clean package ou mvn clean install*

## Rodando Aplicação

###### Para subir aplicação (localmente) inicializar TestUolApiApplication.java
###### Após subida acessar http://localhost:8080/swagger-ui-custom.html onde está disponível O Swagger com os endpoints desenvolvidos.

## Banco de Dados

###### Para visualizar o banco de dados é necessário acessar **_http://localhost:8080/h2-console/_** e logar com as credenciais **_username_**:sa **_password_**:123mudar

