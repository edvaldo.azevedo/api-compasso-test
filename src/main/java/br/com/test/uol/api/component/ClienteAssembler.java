package br.com.test.uol.api.component;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import br.com.test.uol.api.entity.Cidade;
import br.com.test.uol.api.entity.Cliente;
import br.com.test.uol.api.model.ClienteRequestDTO;
import br.com.test.uol.api.model.ClienteResponseDTO;
import br.com.test.uol.api.service.impl.CidadeServiceImpl;
import br.com.test.uol.api.service.impl.ClienteServiceImpl;

@Component
public class ClienteAssembler {

	@Autowired
	private CidadeServiceImpl cidadeService;

	@Autowired
	ClienteServiceImpl clienteService;

	public Cliente clienteCreateFromDTO(ClienteRequestDTO dto) throws Exception {

		Cliente cl = null;
		if (null != dto) {

			cl = new Cliente();
			Cidade c = cidadeService.buscarPorId(dto.getCidade());
			cl.setCidade(null != c ? c : null);
			cl.setIdade(dto.getIdade());
			cl.setDataNascimento(dto.getDataNascimento());
			cl.setNome(dto.getNome());
			cl.setSexo(dto.getSexo());

		}
		return cl;
	}

	public Cliente clienteEditFromDTO(ClienteRequestDTO dto) throws Exception {

		Cliente clAlready = clienteService.buscarPorId(dto.getId());

		if (null != dto && null != clAlready) {

			Cidade c = cidadeService.buscarPorId(dto.getCidade());
			clAlready.setId(c.getId());
			clAlready.setCidade(null != c ? c : null);
			clAlready.setIdade(dto.getIdade());
			clAlready.setDataNascimento(dto.getDataNascimento());
			clAlready.setNome(dto.getNome());
			clAlready.setSexo(dto.getSexo());
		}
		return clAlready;

	}

	public ClienteResponseDTO clienteResponseDTOfromCliente(Cliente cliente) {

		ClienteResponseDTO response = null;
		if (null != cliente) {
			response = new ClienteResponseDTO();
			response.setId(cliente.getId());
			response.setCidade(cliente.getCidade().getNome());
			response.setDataNascimento(cliente.getDataNascimento());
			response.setIdade(cliente.getIdade());
			response.setSexo(cliente.getSexo());
			response.setNome(cliente.getNome());
		}
		return response;
	}

}
