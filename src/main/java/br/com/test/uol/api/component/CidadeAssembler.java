package br.com.test.uol.api.component;

import java.util.ArrayList;

import org.springframework.stereotype.Component;

import antlr.collections.List;
import br.com.test.uol.api.entity.Cidade;
import br.com.test.uol.api.model.CidadeRequestDTO;
import br.com.test.uol.api.model.CidadeResponseDTO;

@Component
public class CidadeAssembler {
	
	public Cidade cidadeCreateFromDTO(CidadeRequestDTO dto) {
		
		Cidade c = null;
		if (dto!=null) {
			c = new Cidade();
			c.setEstado(dto.getEstado());
			c.setNome(dto.getNome());
		}
		return c;
	}
	
	public ArrayList<CidadeResponseDTO> cidadeDTOFromList() {
		return new ArrayList<CidadeResponseDTO>();
	}
	
	public CidadeResponseDTO cidadeResponseFromCidade (Cidade cidade) {
		
		if(null!=cidade) {
			
			CidadeResponseDTO cRDTO = new CidadeResponseDTO();
			
			cRDTO.setEstado(cidade.getEstado());
			cRDTO.setId(cidade.getId());
			cRDTO.setNome(cidade.getNome());
			return cRDTO;
		}
		return null;
	}

	
	
	

}
