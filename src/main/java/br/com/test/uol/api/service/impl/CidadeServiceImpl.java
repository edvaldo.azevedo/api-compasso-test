package br.com.test.uol.api.service.impl;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Example;
import org.springframework.stereotype.Service;

import br.com.test.uol.api.component.CidadeAssembler;
import br.com.test.uol.api.entity.Cidade;
import br.com.test.uol.api.model.CidadeRequestDTO;
import br.com.test.uol.api.model.CidadeResponseDTO;
import br.com.test.uol.api.repository.CidadeRepository;

@Service
public class CidadeServiceImpl {

	@Autowired
	private CidadeRepository cidadeRepository;

	@Autowired
	private CidadeAssembler cidadeAssembler;

	public Cidade cadastrarCidade(CidadeRequestDTO cidade) throws Exception {

		Cidade c = cidade.getId() != null && cidade.getId() != 0 ? cidadeRepository.getOne(cidade.getId()) : null;
		if (c != null) {
			throw new Exception("Cidade já cadastrada");
		}
		c = cidadeAssembler.cidadeCreateFromDTO(cidade);
		return cidadeRepository.save(c);

	}

	public List<Cidade> buscarCidadesPorFiltro(Example<Cidade> example) {

		return cidadeRepository.findAll(example);

	}

	public CidadeResponseDTO buscarCidadePorNome(String nome) {

		return cidadeAssembler.cidadeResponseFromCidade(cidadeRepository.findByNomeIgnoreCase(nome));
	}

	public CidadeResponseDTO buscarCidadePorEstado(String estado) {

		return cidadeAssembler.cidadeResponseFromCidade(cidadeRepository.findByEstadoIgnoreCase(estado));
	}
	
	public Cidade buscarPorId(Long id) throws Exception {

		Optional<Cidade> c = cidadeRepository.findById(id);
		
		if (!c.isEmpty()) {
			return c.get();
		}
		throw new Exception("Nenhuma cidade encontrada para este id");
	}

	

}
