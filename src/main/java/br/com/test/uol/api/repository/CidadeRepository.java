package br.com.test.uol.api.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import br.com.test.uol.api.entity.Cidade;

@Repository
public interface CidadeRepository extends JpaRepository<Cidade, Long> {

	Cidade findByNomeIgnoreCase(String nome);

	Cidade findByEstadoIgnoreCase(String estado);

}
