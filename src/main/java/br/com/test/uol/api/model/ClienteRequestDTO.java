package br.com.test.uol.api.model;

import java.time.LocalDate;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

import lombok.Data;

@Data
public class ClienteRequestDTO {
	
	
	private Long id;
	
	@NotBlank(message = "campo nome não pode ser nulo ou vazio")
	private String nome;
	
	@NotBlank(message = "campo sexo não pode ser nulo ou vazio")
	private String sexo;
	
	private LocalDate dataNascimento;
	
	@NotNull(message = "campo idade não pode ser nulo ou vazio")
	private int idade;
	
	@NotNull(message = "campo cidade não pode ser nulo ou vazio")
	private Long cidade;
	
	
	

}
