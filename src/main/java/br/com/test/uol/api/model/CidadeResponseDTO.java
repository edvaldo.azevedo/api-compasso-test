package br.com.test.uol.api.model;

import lombok.Data;

@Data
public class CidadeResponseDTO {
	
	private Long id;
	private String nome;
	private String estado;

}
