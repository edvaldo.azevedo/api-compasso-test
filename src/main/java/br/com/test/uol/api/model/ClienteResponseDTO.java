package br.com.test.uol.api.model;

import java.time.LocalDate;

import lombok.Data;

@Data
public class ClienteResponseDTO {

	
	private Long id;
	
	private String cidade;
	
	private String nome;
	
	private int idade;
	
	private LocalDate dataNascimento;
	
	private String sexo;
}
