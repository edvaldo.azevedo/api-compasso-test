package br.com.test.uol.api;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TestUolApiApplication {

	public static void main(String[] args) {
		SpringApplication.run(TestUolApiApplication.class, args);
	}

}
