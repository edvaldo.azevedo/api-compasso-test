package br.com.test.uol.api.model;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;

import lombok.Data;

@Data
public class CidadeRequestDTO {
	
	private Long id;
	
	@NotBlank(message = "campo nome não pode ser nulo ou vazio")
	private String nome;
	
	@NotBlank(message = "campo estado não pode ser nulo ou vazio")
	private String estado;
}
