package br.com.test.uol.api.controller;

import java.util.List;
import java.util.stream.Collectors;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.ExampleMatcher;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import br.com.test.uol.api.entity.Cidade;
import br.com.test.uol.api.model.CidadeRequestDTO;
import br.com.test.uol.api.model.CidadeResponseDTO;
import br.com.test.uol.api.service.impl.CidadeServiceImpl;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.tags.Tag;

@RestController
@RequestMapping("/api/cidade")
@Tag(description = "enpoint para acesso de recursos cidade", name = "Cidade")
public class CidadeController {

	@Autowired
	private CidadeServiceImpl cidadeService;

	@PostMapping(name = "/cadastrar", headers = "Accept=application/json")
	@Operation(description = "endpoint para cadastro de cidades")
	@ResponseStatus(HttpStatus.OK)
	public ResponseEntity<?> cadastrarCidade(@Valid @RequestBody(required = true) CidadeRequestDTO cidade,
			BindingResult result) throws Exception {

		if (result.hasErrors()) {
			List<String> errors = result.getAllErrors().stream().map(e -> e.getDefaultMessage())
					.collect(Collectors.toList());
			return ResponseEntity.badRequest().body(errors);

		}
		return ResponseEntity.ok(cidadeService.cadastrarCidade(cidade));
	}

	@GetMapping("/filtro")
	@Operation(description = "endpoint para buscar cidade por filtros")
	public ResponseEntity<List<Cidade>> buscarCidadesPorFiltro(@Parameter @RequestParam(required = false) String nome,
			@Parameter @RequestParam(required = false) String estado) {

		Cidade c = Cidade.builder().nome(nome).estado(estado).build();

		ExampleMatcher exMatcher = ExampleMatcher.matchingAll().withIgnoreCase();
		Example<Cidade> example = Example.of(c, exMatcher);

		return ResponseEntity.ok(cidadeService.buscarCidadesPorFiltro(example));
	}

	@GetMapping(value = "/nome", headers = "Accept=application/json")
	@Operation(description = "endpoint para buscar cidade pelo nome")
	public ResponseEntity<CidadeResponseDTO> buscarCidadePorNome(
			@Parameter @RequestParam(required = false) String nome) {

		return ResponseEntity.ok(cidadeService.buscarCidadePorNome(nome));
	}

	@GetMapping("/estado")
	@Operation(description = "endpoint para buscar cidade pelo estado")
	public ResponseEntity<CidadeResponseDTO> buscarCidadePorEstado(
			@Parameter @RequestParam(required = false) String estado) {

		return ResponseEntity.ok(cidadeService.buscarCidadePorEstado(estado));
	}

}
