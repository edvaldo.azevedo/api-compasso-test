package br.com.test.uol.api.controller;

import java.util.List;
import java.util.stream.Collectors;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import br.com.test.uol.api.entity.Cliente;
import br.com.test.uol.api.model.ClienteRequestDTO;
import br.com.test.uol.api.model.ClienteResponseDTO;
import br.com.test.uol.api.service.impl.ClienteServiceImpl;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.tags.Tag;

@RestController
@RequestMapping("/api/cliente")
@Tag(description = "enpoint para acesso de recursos cliente", name = "Cliente")
public class ClienteController {
	
	@Autowired
	private ClienteServiceImpl clienteService;
	
	@PostMapping(value = "/cadastrar", headers = "Accept=application/json")
	@Operation(description = "recurso para cadastro de clientes")
	public ResponseEntity<Object> cadastrarCliente (@Valid @RequestBody(required = true) ClienteRequestDTO dto, BindingResult result) throws Exception{
		
		if (result.hasErrors()) {
			List<String> errors = result.getAllErrors().stream().map(e -> e.getDefaultMessage())
					.collect(Collectors.toList());
			return ResponseEntity.badRequest().body(errors);

		}
		
		return ResponseEntity.ok(clienteService.cadastrarCliente(dto));
		
	}
	
	 @GetMapping("/listar/nome")
	 @Operation(description = "endpoint para buscar cliente pelo nome")
	 public ResponseEntity<ClienteResponseDTO> buscarClientePorNome(@Parameter @RequestParam(required = false) String nome){
		 
		 return ResponseEntity.ok(clienteService.buscarClientePorNome(nome));
	 }
	 
	 @GetMapping("/listar/{id}")
	 @Operation(description = "endpoint para buscar cliente por id")
	 public ResponseEntity<ClienteResponseDTO> buscarClientePorId(@Parameter @PathVariable(required = false, name = "id") Long id) throws Exception{
		 
		 return ResponseEntity.ok(clienteService.buscarClientePorId(id));
	 }
	 
	 @DeleteMapping("/remover/{id}")
	 @Operation(description = "endpoint para remover o cliente")
	 public ResponseEntity<?> removerCliente(@PathVariable("id")Long id) throws Exception{
		 
		 return ResponseEntity.ok(clienteService.removerCliente(id));
	 }
	 
	 @PutMapping("/atualizar")
	 @Operation(description = "endpoint para atualizar dados do cliente")
	 public ResponseEntity<?> atualizarCliente(@RequestBody(required = true) ClienteRequestDTO dto) throws Exception{
		return ResponseEntity.ok(clienteService.atualizarCliente(dto));
		 
	 }

}
