package br.com.test.uol.api.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import br.com.test.uol.api.entity.Cliente;

@Repository
public interface ClienteRepository  extends JpaRepository<Cliente, Long>{

	Cliente findByNomeIgnoreCase(String nome);

}
