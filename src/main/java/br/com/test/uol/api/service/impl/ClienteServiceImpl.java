package br.com.test.uol.api.service.impl;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.test.uol.api.component.ClienteAssembler;
import br.com.test.uol.api.entity.Cidade;
import br.com.test.uol.api.entity.Cliente;
import br.com.test.uol.api.model.ClienteRequestDTO;
import br.com.test.uol.api.model.ClienteResponseDTO;
import br.com.test.uol.api.repository.ClienteRepository;

@Service
public class ClienteServiceImpl {

	@Autowired
	private ClienteRepository clienteRepository;

	@Autowired
	private ClienteAssembler clienteAssembler;

	public Cliente cadastrarCliente(ClienteRequestDTO dto) throws Exception {

		Cliente cl = null != dto.getNome() ? clienteRepository.findByNomeIgnoreCase(dto.getNome()) : null;
		if (null != cl) {
			throw new Exception("Já existe cliente com este nome");
		}
		cl = clienteAssembler.clienteCreateFromDTO(dto);

		return clienteRepository.save(cl);

	}

	public ClienteResponseDTO buscarClientePorNome(String nome) {

		Cliente c = clienteRepository.findByNomeIgnoreCase(nome);

		return null != c ? clienteAssembler.clienteResponseDTOfromCliente(c) : null;
	}

	public ClienteResponseDTO buscarClientePorId(Long id) throws Exception {

		Optional<Cliente> c = clienteRepository.findById(id);

		if (!c.isEmpty()) {
			return clienteAssembler.clienteResponseDTOfromCliente(c.get());
		}
		throw new Exception("Nenhum cliente encontrado para o id");
	}

	public Object removerCliente(Long id) throws Exception {
		Optional<Cliente> c = clienteRepository.findById(id);
		if (c.isEmpty()) {
			throw new Exception("Nenhum cliente encontrado para o id");
		}
		clienteRepository.delete(c.get());
		return c.get();
	}

	public Cliente atualizarCliente(ClienteRequestDTO dto) throws Exception {

		Cliente cl = clienteAssembler.clienteEditFromDTO(dto);

		return clienteRepository.save(cl);

	}

	public Cliente buscarPorId(Long id) throws Exception {

		Optional<Cliente> c = clienteRepository.findById(id);

		if (c.isEmpty()) {
			throw new Exception("Cliente não encontado");
		}
		return c.get();

	}

}
