package br.com.test.uol.api.controller;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;

import java.time.LocalDate;

import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import br.com.test.uol.api.component.ClienteAssembler;
import br.com.test.uol.api.entity.Cidade;
import br.com.test.uol.api.entity.Cliente;
import br.com.test.uol.api.model.CidadeResponseDTO;
import br.com.test.uol.api.model.ClienteRequestDTO;
import br.com.test.uol.api.model.ClienteResponseDTO;
import br.com.test.uol.api.service.impl.ClienteServiceImpl;

@SpringBootTest
@AutoConfigureMockMvc
public class ClienteControllerTest {
	
	
	@Autowired
	private MockMvc mockMvc;

	@MockBean
	private ClienteServiceImpl clienteService;
	
	@MockBean
	private ClienteAssembler clienteAssembler;
	
	
	@Test
	public void chamandoCadastrarCliente() throws Exception {

		ClienteRequestDTO dto= new ClienteRequestDTO();
		dto.setNome("Edvaldo Azevedo");
		dto.setSexo("Masculino");
		dto.setIdade(26);
		dto.setDataNascimento(LocalDate.of(1994, 06, 06));
		dto.setCidade(1L);

		Cliente cl = Cliente.builder().cidade(new Cidade()).build();
		String json = "{\"nome\":\"Edvaldo Azevedo\",\"sexo\":\"Masculino\"}";
		Mockito.when(clienteService.cadastrarCliente(dto)).thenReturn(cl);
		this.mockMvc.perform(post("/api/cliente/cadastrar").content(json).accept("application/json")
				.contentType("application/json")).andExpect(MockMvcResultMatchers.status().isOk());

	}
	
	@Test
	public void chamandoBuscaDeClientePorNome() throws Exception {

		ClienteResponseDTO dto= new ClienteResponseDTO();
		dto.setNome("Edvaldo Azevedo");
		dto.setSexo("Masculino");
		dto.setIdade(26);
		dto.setDataNascimento(LocalDate.of(1994, 06, 06));
		dto.setCidade("Areial");

		
		Mockito.when(clienteService.buscarClientePorNome(dto.getNome())).thenReturn(dto);
		this.mockMvc.perform(get("/api/cliente/listar/nome").queryParam("nome", dto.getNome()).accept("application/json")).andExpect(MockMvcResultMatchers.status().is2xxSuccessful());

	}
	
	

}
