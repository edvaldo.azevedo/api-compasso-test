package br.com.test.uol.api.controller;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;

import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import br.com.test.uol.api.component.CidadeAssembler;
import br.com.test.uol.api.entity.Cidade;
import br.com.test.uol.api.model.CidadeRequestDTO;
import br.com.test.uol.api.model.CidadeResponseDTO;
import br.com.test.uol.api.service.impl.CidadeServiceImpl;

@SpringBootTest
@AutoConfigureMockMvc
public class CidadeControllerTest {

	@Autowired
	private MockMvc mockMvc;

	
	@MockBean
	private CidadeServiceImpl cidadeService;

	@MockBean
	private CidadeAssembler cidadeAssembler;
	
	

	@Test
	public void chamandoCadastrarCidade() throws Exception {

		CidadeRequestDTO dto = new CidadeRequestDTO();
		dto.setEstado("Paraiba");
		dto.setNome("João Pessoa");
		dto.setId(0L);

		Cidade c = Cidade.builder().estado(dto.getEstado()).nome(dto.getNome()).build();

		String json = "{\"nome\":\"Greetting\",\"estado\":\"Hello world\"}";
		Mockito.when(cidadeService.cadastrarCidade(dto)).thenReturn(c);
		this.mockMvc.perform(post("/api/cidade/cadastrar").content(json).accept("application/json")
				.contentType("application/json")).andExpect(MockMvcResultMatchers.status().isNotFound());

	}
	
	@Test
	public void chamandoBuscaDeCidadePorNome() throws Exception {

		CidadeResponseDTO dto = new CidadeResponseDTO();
		dto.setEstado("Paraiba");
		dto.setNome("João Pessoa");
		dto.setId(0L);

		String nome = "Areial";
		
		Mockito.when(cidadeService.buscarCidadePorNome(nome)).thenReturn(dto);
		this.mockMvc.perform(get("/api/cidade/nome").queryParam("nome", nome).accept("application/json")).andExpect(MockMvcResultMatchers.status().is2xxSuccessful());

	}
	
	@Test
	public void chamandoBuscaDeCidadePorEstado() throws Exception {

		CidadeResponseDTO dto = new CidadeResponseDTO();
		dto.setEstado("Paraiba");
		dto.setNome("João Pessoa");
		dto.setId(0L);
		
		String estado = "Paraiba";

		Mockito.when(cidadeService.buscarCidadePorEstado(estado)).thenReturn(dto);
		this.mockMvc.perform(get("/api/cidade/estado").queryParam("estado", estado).accept("application/json")).andExpect(MockMvcResultMatchers.status().is2xxSuccessful());

	}

}
